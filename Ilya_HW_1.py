def get_denominator():
    try:
        read_demon = open('demon.txt', 'r')
        read_data = read_demon.read()
        int_data = int(read_data)
        if int_data == 0:
            raise ZeroDivisionError
        read_demon.close()
        return int_data

    except FileNotFoundError as notfile:
        print(str(notfile), '\nК сожалению файла с таким названием не найдено')
        exit()
    except ValueError as notvalue:
        print(str(notvalue), '\nВ вашем файле находится не число!')
        exit()
    except ZeroDivisionError:
        print("Число в вашем файле это '0'!")
        exit()


def get_list_of_numbers(denominator):
    list_numbers = [x for x in range(1, 101) if x % denominator == 0]
    return list_numbers


def get_sum(list_of_numbers):
    list_sum = sum(list_of_numbers)
    return list_sum


def write_result(number):
    write_result = open('result.txt', 'w')
    write_result.write(str(number))
    write_result.close()


write_result(get_sum(get_list_of_numbers(get_denominator())))
